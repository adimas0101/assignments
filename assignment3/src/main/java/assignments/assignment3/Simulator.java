package assignments.assignment3;

import java.util.Scanner;

public class Simulator{
  	//Ini adalah class utama
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        String inputFile = "NOTHING";
        String outputFile = "NOTHING";
        System.out.println("Selamat datang di program simulasi COVID-19!");
        System.out.println("Silahkan masukkan metode input yang anda inginkan (TEXT/TERMINAL):");

        String inputType = scanner.nextLine();
        if(inputType.equalsIgnoreCase("text")){
            System.out.println("Silahkan masukan full directoy file:");
            inputFile = scanner.nextLine();
        }

        System.out.println("Silahkan masukkan metode output yang anda inginkan (TEXT/TERMINAL):");

        String outputType = scanner.nextLine();
        if(outputType.equalsIgnoreCase("text")){
            System.out.println("Silahkan masukan directory Output file:");
            outputFile = scanner.nextLine();
        }

        InputOutput io = new InputOutput(inputType, inputFile, outputType, outputFile);

        try {
            io.run();
        }
        catch (Exception e){
            e.printStackTrace();
        }
        scanner.close();
    }
}