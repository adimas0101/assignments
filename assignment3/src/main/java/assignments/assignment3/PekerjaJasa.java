package assignments.assignment3;

public class PekerjaJasa extends Manusia{

    /**
     * Constructor, memanggil super class
     * @param nama nama
     */
    public PekerjaJasa(String nama){
    	// TODO: Buat constructor untuk Jurnalis.
        // Hint: Akses constructor superclass-nya
        super(nama, "PEKERJA JASA");
    }
  	
}