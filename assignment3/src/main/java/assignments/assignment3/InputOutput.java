package assignments.assignment3;

import java.io.*;

public class InputOutput{
  	
    private BufferedReader br;
    private PrintWriter pw;
    private String inputFile;
    private String outputFile; 
    private World world;
    String toGUI;

    /**
     * Constructor dari InputOutput, memasukan inputfile dan outputfile, serta memanggil setBuffered dan setprintwriter
     * @param inputType tipe input
     * @param inputFile file path
     * @param outputType tipe output
     * @param outputFile output path
     */
    public InputOutput(String inputType, String inputFile, String outputType, String outputFile){
        // TODO: Buat constructor untuk InputOutput.
        this.inputFile = inputFile;
        this.outputFile = outputFile;

        setBufferedReader(inputType);
        setPrintWriter(outputType);
        world = new World();
    }

    /**
     * Mensetting input, apakah dari file atau dari terminal
     * @param inputType (terminal/text)
     */
    public void setBufferedReader(String inputType) {
        // TODO: Membuat BufferedReader bergantung inputType (I/O text atau input terminal)

        if (inputType.equalsIgnoreCase("text")) {
            try {
                FileReader fileInput = new FileReader(inputFile);
                br = new BufferedReader(fileInput);
            } catch (Exception e) {
                System.out.println(e);
                e.printStackTrace();
            }
        } else if (inputType.equalsIgnoreCase("JavaFx GUI")){


        } else {
            System.out.println("Query:");
            InputStreamReader inputStreamReader = new InputStreamReader(System.in);
            br = new BufferedReader(inputStreamReader);
        }
    }

    /**
     * Mensetting output, apakah dari file atau dari terminal
     * @param outputType (text/terminal)
     */
    public void setPrintWriter(String outputType) {
        // TODO: Membuat PrintWriter bergantung inputType (I/O text atau output terminal)
        if (outputType.equals("text")) {
            try {
                File file = new File(outputFile);
                if (!file.exists()) {
                    file.createNewFile();
                }
                pw = new PrintWriter(file);
            } catch (IOException e) {
                e.printStackTrace();
            }
        } else if (outputType.equalsIgnoreCase("JavaFx GUI")) {
            toGUI = "";

        } else {
            pw = new PrintWriter(System.out);
        }
    }


    /**
     * Menjalankan dan menyeleksi setiap querry atau perintah yang diberikan
     * @throws IOException error
     */
    public void run() throws IOException{
        // TODO: Program utama untuk InputOutput, jangan lupa handle untuk IOException
        // Hint: Buatlah objek dengan class World
        // Hint: Untuk membuat object Carrier baru dapat gunakan method CreateObject pada class World
        // Hint: Untuk mengambil object Carrier dengan nama yang sesua dapat gunakan method getCarrier pada class World
        String line;
        try {
            while ((line = br.readLine()) != null) {

                String[] kata = line.split(" ");

                if (kata[0].equalsIgnoreCase("add")) {
                    if (world.getCarrier(kata[2]) == null) {
                        world.createObject(kata[1], kata[2]);
                    }
                } else if (kata[0].equalsIgnoreCase("interaksi")) {
                    Carrier objek1 = world.getCarrier(kata[1]);
                    Carrier objek2 = world.getCarrier(kata[2]);
                    if (objek1 != null && objek2 != null) {
                        objek1.interaksi(objek2);
                    }
                } else if (kata[0].equalsIgnoreCase("positifkan")) {
                    Carrier objek1 = world.getCarrier(kata[1]);
                    if (objek1 instanceof Manusia)
                        ((Manusia) objek1).terinfeksi();

                } else if (kata[0].equalsIgnoreCase("sembuhkan")) {
                    Carrier objek1 = world.getCarrier(kata[1]);
                    Carrier objek2 = world.getCarrier(kata[2]);
                    if (objek1 != null && objek2 != null) {
                        if (objek1 instanceof PetugasMedis && objek2 instanceof Manusia && objek2.getStatusCovid().equals("Positif")) {
                            ((PetugasMedis) objek1).obati((Manusia) objek2);
                        }
                    }

                } else if (kata[0].equalsIgnoreCase("bersihkan")) {
                    Carrier objek1 = world.getCarrier(kata[1]);
                    Carrier objek2 = world.getCarrier(kata[2]);
                    if (objek1 instanceof CleaningService && objek2 instanceof Benda) {
                        ((CleaningService) objek1).bersihkan((Benda) objek2);
                    }

                } else if (kata[0].equalsIgnoreCase("rantai")) {
                    Carrier objek1 = world.getCarrier(kata[1]);
                    try {
                        checkBelumTertular((Manusia) objek1);

                        String result = String.format("Rantai penyebaran %s: ", objek1);

                        for (Carrier carrier : objek1.getRantaiPenular()) {
                            result += String.format("%s -> ", carrier);
                        }
                        result = result.substring(0, (result.length() - 4));
                        pw.println(result);

                    } catch (BelumTertularException e) {
                        pw.println(e);
                    }

                } else if (kata[0].equalsIgnoreCase("total_kasus_dari_objek")) {
                    Carrier objek1 = world.getCarrier(kata[1]);
                    if (objek1 != null) {
                        pw.printf("%s telah menyebarkan virus COVID ke %d objek\n", objek1, objek1.getTotalKasusDisebabkan());
                    }

                } else if (kata[0].equalsIgnoreCase("aktif_kasus_dari_objek")) {
                    Carrier objek1 = world.getCarrier(kata[1]);
                    if (objek1 != null) {
                        pw.printf("%s telah menyebarkan virus COVID dan masih teridentifikasi positif sebanyak %d objek\n", objek1, objek1.getAktifKasusDisebabkan());
                    }

                } else if (kata[0].equalsIgnoreCase("total_sembuh_manusia")) {
                    pw.printf("Total sembuh dari kasus COVID yang menimpa manusia ada sebanyak: %d kasus\n", Manusia.getJumlahSembuh());

                } else if (kata[0].equalsIgnoreCase("total_sembuh_petugas_medis")) {
                    Carrier objek1 = world.getCarrier(kata[1]);
                    if (objek1 instanceof PetugasMedis) {
                        pw.printf("%s menyembuhkan %d manusia\n", objek1, ((PetugasMedis) objek1).getJumlahDisembuhkan());
                    }

                } else if (kata[0].equalsIgnoreCase("total_bersih_cleaning_service")) {
                    Carrier objek1 = world.getCarrier(kata[1]);
                    if (objek1 instanceof CleaningService) {
                        pw.printf("%s membersihkan %d benda\n", objek1, ((CleaningService) objek1).getJumlahDibersihkan());
                    }

                } else if (kata[0].equalsIgnoreCase("exit")) {
                    break;
                }
            }
        } catch (Exception e){
            e.printStackTrace();
        }
        pw.close();
    }

    /**
     * Mengecek apakah manusia tersebut negatif atau positif
     * @param manusia manusia
     * @throws BelumTertularException belumtertular
     */
    private void checkBelumTertular(Manusia manusia) throws BelumTertularException {
        if (manusia.getStatusCovid().equals("Negatif"))
            throw new BelumTertularException(String.format("%s berstatus negatif", manusia));
    }

    //++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

    /**
     * getter world
     * @return World
     */
    public World getWorld(){
        return this.world;
    }

    /**
     * Method untuk menyeseleksi perintah dari GUI
     * @param command perintah
     * @throws IOException error
     */
    public void GUI(String command) throws IOException{
        // TODO: Program utama untuk InputOutput, jangan lupa handle untuk IOException
        // Hint: Buatlah objek dengan class World
        // Hint: Untuk membuat object Carrier baru dapat gunakan method CreateObject pada class World
        // Hint: Untuk mengambil object Carrier dengan nama yang sesua dapat gunakan method getCarrier pada class World
        String line;
        try {
            line = command;
            String[] kata = line.split(" ");


            if (kata[0].equalsIgnoreCase("add")) {
                if (world.getCarrier(kata[2]) == null) {
                    world.createObject(kata[1], kata[2]);
                    toGUI += String.format("Berhasil Menambah %s\n", world.getCarrier(kata[2]));
                }
            } else if (kata[0].equalsIgnoreCase("interaksi")) {
                Carrier objek1 = world.getCarrier(kata[1]);
                Carrier objek2 = world.getCarrier(kata[2]);
                if (objek1 != null && objek2 != null) {
                    objek1.interaksi(objek2);
                    toGUI += String.format("%s Berinteraksi dengan %s\n", world.getCarrier(kata[1]), world.getCarrier(kata[2]));
                }
            } else if (kata[0].equalsIgnoreCase("positifkan")) {
                Carrier objek1 = world.getCarrier(kata[1]);
                if (objek1 instanceof Manusia) {
                    ((Manusia) objek1).terinfeksi();
                    toGUI += String.format("%s telah terinveksi Covid-19\n", objek1);
                }
            } else if (kata[0].equalsIgnoreCase("sembuhkan")) {
                Carrier objek1 = world.getCarrier(kata[1]);
                Carrier objek2 = world.getCarrier(kata[2]);
                if (objek1 != null && objek2 != null) {
                    if (objek1 instanceof PetugasMedis && objek2 instanceof Manusia && objek2.getStatusCovid().equals("Positif")) {
                        ((PetugasMedis) objek1).obati((Manusia) objek2);
                        toGUI += String.format("%s telah menyembuhkan %s\n", objek1, objek2);
                    }
                }

            } else if (kata[0].equalsIgnoreCase("bersihkan")) {
                Carrier objek1 = world.getCarrier(kata[1]);
                Carrier objek2 = world.getCarrier(kata[2]);
                if (objek1 instanceof CleaningService && objek2 instanceof Benda) {
                    ((CleaningService) objek1).bersihkan((Benda) objek2);
                    toGUI += String.format("%s telah membersihkan %s\n", objek1, objek2);
                }

            } else if (kata[0].equalsIgnoreCase("rantai")) {
                Carrier objek1 = world.getCarrier(kata[1]);
                try {
                    checkBelumTertular((Manusia) objek1);

                    String result = String.format("Rantai penyebaran %s: ", objek1);

                    for (Carrier carrier : objek1.getRantaiPenular()) {
                        result += String.format("%s -> ", carrier);
                    }
                    result = result.substring(0, (result.length() - 4));
                    toGUI += (result + "\n");

                } catch (BelumTertularException e) {
                    toGUI += (e + "\n");
                }

            } else if (kata[0].equalsIgnoreCase("total_kasus_dari_objek")) {
                Carrier objek1 = world.getCarrier(kata[1]);
                if (objek1 != null) {
                    toGUI += String.format("%s telah menyebarkan virus COVID ke %d objek\n", objek1, objek1.getTotalKasusDisebabkan());
                }

            } else if (kata[0].equalsIgnoreCase("aktif_kasus_dari_objek")) {
                Carrier objek1 = world.getCarrier(kata[1]);
                if (objek1 != null) {
                    toGUI += String.format("%s telah menyebarkan virus COVID dan masih teridentifikasi positif sebanyak %d objek\n", objek1, objek1.getAktifKasusDisebabkan());
                }

            } else if (kata[0].equalsIgnoreCase("total_sembuh_manusia")) {
                toGUI += String.format("Total sembuh dari kasus COVID yang menimpa manusia ada sebanyak: %d kasus\n", Manusia.getJumlahSembuh());

            } else if (kata[0].equalsIgnoreCase("total_sembuh_petugas_medis")) {
                Carrier objek1 = world.getCarrier(kata[1]);
                if (objek1 instanceof PetugasMedis) {
                    toGUI += String.format("%s menyembuhkan %d manusia\n", objek1, ((PetugasMedis) objek1).getJumlahDisembuhkan());
                }

            } else if (kata[0].equalsIgnoreCase("total_bersih_cleaning_service")) {
                Carrier objek1 = world.getCarrier(kata[1]);
                if (objek1 instanceof CleaningService) {
                    toGUI += String.format("%s membersihkan %d benda\n", objek1, ((CleaningService) objek1).getJumlahDibersihkan());
                }

            } else if (kata[0].equalsIgnoreCase("exit")) {

            }

        } catch (Exception e){
            e.printStackTrace();
        }

    }
}