package assignments.assignment2;

public class ButirPenilaian {
    private static final int PENALTI_KETERLAMBATAN = 20;
    private double nilai;
    private boolean terlambat;

    /**
     * Membuat constructor untuk ButirPenilaian.
     * @param nilai nilai dari butir.
     * @param terlambat param keterlambatan.
     */
    public ButirPenilaian(double nilai, boolean terlambat) {
        this.nilai = nilai;
        this.terlambat = terlambat;
        if (this.terlambat) {
            this.nilai -= this.nilai * (PENALTI_KETERLAMBATAN / 100.0);
        }
        if (nilai < 0) {
            this.nilai = 0;
        }
    }

    /**
     * Mengembalikan nilai yang sudah disesuaikan dengan keterlambatan.
     * @return nilai yang sudah dikurangi penalti.
     */
    public double getNilai() {
        return nilai;
    }

    /**
     * Megembalikan representasi String dari ButirPenilaian sesuai permintaan soal.
     */
    @Override
    public String toString() {
        return String.format("%.2f", nilai) + (!(terlambat) ? "" : " (T)");
    }
}
