package assignments.assignment2;

public class KomponenPenilaian {
    private String nama;
    private ButirPenilaian[] butirPenilaian;
    private int bobot;

    /**
     * Membuat constructor untuk KomponenPenilaian.
     * Note: banyakButirPenilaian digunakan untuk menentukan panjang butirPenilaian saja.
     * (tanpa membuat objek-objek ButirPenilaian-nya).
     * @param nama komponen penilaian.
     * @param banyakButirPenilaian banyak butir penilaian.
     * @param bobot dari komponen.
     */
    public KomponenPenilaian(String nama, int banyakButirPenilaian, int bobot) {
        this.nama = nama;
        this.bobot = bobot;
        this.butirPenilaian = new ButirPenilaian[banyakButirPenilaian];
    }

    /**
     * Membuat objek KomponenPenilaian baru berdasarkan bentuk KomponenPenilaian templat.
     * @param templat templat KomponenPenilaian.
     */
    private KomponenPenilaian(KomponenPenilaian templat) {
        this(templat.nama, templat.butirPenilaian.length, templat.bobot);
    }

    /**
     * Mengembalikan salinan skema penilaian berdasarkan templat yang diberikan.
     * @param templat templat skema penilaian sebagai sumber.
     * @return objek baru yang menyerupai templat.
     */
    public static KomponenPenilaian[] salinTemplat(KomponenPenilaian[] templat) {
        KomponenPenilaian[] salinan = new KomponenPenilaian[templat.length];
        for (int i = 0; i < salinan.length; i++) {
            salinan[i] = new KomponenPenilaian(templat[i]);
        }
        return salinan;
    }

    /**
     * Memasukkan butir ke butirPenilaian pada index ke-idx.
     * @param idx index komponen.
     * @param butir butir penilaian.
     */
    public void masukkanButirPenilaian(int idx, ButirPenilaian butir) {
        butirPenilaian[idx] = butir;
    }

    /**
     * Mengembalikan nama KomponenPenilaian.
     * @return nama komponen penilaian.
     */
    public String getNama() {
        return nama;
    }

    /**
     * Mengembaikan rata-rata dari komponen penilaian.
     * @return rerata dari komponen.
     */
    public double getRerata() {
        // Mengembalikan rata-rata butirPenilaian.
        double total = 0;
        int count = 0;
        for (ButirPenilaian nilai : butirPenilaian) {
            if (nilai != null) {
                count++;
                total += nilai.getNilai();
            }
        }
        // Jika asdos belum menginput nilai sama sekali
        if (count == 0) {
            return 0;
        }
        return total / count;
    }

    /**
     * Mengembalikan rerata yang sudah dikalikan dengan bobot.
     * @return Nilai dari komponen.
     */
    public double getNilai() {
        return getRerata() * (bobot / 100.0);
    }

    /**
     * Mengembalikan detail KomponenPenilaian sesuai permintaan soal.
     * @return result dari getDetail seusai format.
     */
    public String getDetail() {
        int count = 0;
        String result = String.format("~~~ %s (%d%%) ~~~\n", nama, bobot);

        if (butirPenilaian.length > 1) {
            for (ButirPenilaian nilai : butirPenilaian) {
                if (nilai != null) {
                    count++;
                    result += String.format("%s %d: %s \n", nama, count, nilai);
                }
            }
            if (count == 0) {
                result += "Rerata: 0.00 \n";
            }

        } else {
            result += String.format("%s: %s \n", nama,
                    (butirPenilaian[0] != null) ? butirPenilaian[0] : "0.00");
        }

        if (count > 1) {
            result += String.format("Rerata: %.2f \n", getRerata());
        }

        result += String.format("Kontribusi nilai akhir: %.2f", getNilai());
        return result;
    }

    /**
     * Mengembalikan representasi String sebuah KomponenPenilaian sesuai permintaan soal.
     */
    @Override
    public String toString() {
        if (butirPenilaian.length > 1) {
            return String.format("Rerata %s: %.2f", nama, getRerata());
        } else {
            return String.format("%s: %.2f", nama, getRerata());
        }
    }

}
